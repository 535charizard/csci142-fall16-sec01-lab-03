package testcases;

import static org.junit.Assert.*;
import org.junit.Test;
import structures.LinkedList;

public class testContain {
	private LinkedList link;
	
	@Test
	public void containsOneNode()
	{
		link = new LinkedList();
		link.addLast("I");
		link.addLast("am");
		link.addLast("Strong");
		
		assertFalse("contains method returns true",link.contains("I") == false );
		assertFalse("contains method returns true",link.contains("am") == false );
		assertTrue("contains method returns false",link.contains("Strong") == true );
	}
	
	@Test
	public void containsTwoNodes()
	{
		link = new LinkedList();
		link.addFirst("A");
		link.addFirst("B");
		link.addFirst("C");
		link.addFirst("D");
		link.addLast("P");
		link.addLast("Q");
		
		assertTrue("Contains is not equall to D",link.contains("D") == true);
		assertTrue("Contains is not equall to P",link.contains("P") == true);
	}

	@Test
	public void containsManyNodes()
	{
		link = new LinkedList();
		link.addFirst("One");
		link.addFirst("Two");
		link.addFirst("Three");
		link.addFirst("Three");
		link.addFirst("Four");
		link.addFirst("Five");
		link.addLast("Six");
		link.addLast("Six");
		link.addLast("Seven");
		link.addLast("Eight");
		link.addLast("Ten");
		
		assertFalse("This contains does find Three and the given Object as equall",link.contains("Twenty"));
		assertFalse("This contains does find Five and the given Object as equall",link.contains("One Hundred"));
		assertTrue("This contains does not find Seven and the Object given equalls",link.contains("Seven"));
		assertTrue("This contains does not find the object it is looking for",link.contains("Three"));
		assertTrue("This contains does not finds the Object it is looking for",link.contains("One"));	
		assertTrue("This contains does not finds the Object it is looking for",link.contains("Five"));
		assertTrue("This contains does not finds the Object it is looking for",link.contains("Ten"));
	}
}
