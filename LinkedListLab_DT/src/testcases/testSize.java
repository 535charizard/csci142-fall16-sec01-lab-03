package testcases;
import static org.junit.Assert.*;
import org.junit.Test;
import structures.LinkedList;

public class testSize {

private LinkedList link;


	
	@Test
	public void sizeOfThreeNodes()
	{
		link = new LinkedList();
		link.addLast("Abc");
		link.addLast("Bbb");
		link.addLast("Ccc");
		System.out.println("Size" + link.size());
		assertTrue("Didn't returns Size of 3",link.size() == 3);
	}
	
	@Test
	public void sizeOfNoNodes()
	{
		link = new LinkedList();
		assertTrue("Didn't returns Size of 0",link.size() == 0);
	}
	
	@Test
	public void sizeOfManyNodes()
	{
	    link = new LinkedList();
	    link.addLast("Abc");
		link.addLast("Bbb");
		link.addLast("Ccc");
		link.addFirst("Ddd");
		link.addFirst("Eee");
		
		assertTrue("Didn't Returns Size of 5",link.size() == 5);
	}
	
	@Test
	public void sizeGrowingThenShrinking()
	{
		link = new LinkedList();
	    link.addLast(1);
		link.addLast(2);
		link.addLast(3);
		link.addLast(4);
		link.addLast(5);
		link.addLast(6);
		assertTrue("Size is not equall to 6",link.size() == 6);
		link.removeLast();
		link.removeLast();
		link.removeLast();
		assertFalse("Size is equall to 6",link.size() == 6);
		assertTrue("Size is not equall to 3",link.size() == 3);
	}
}
