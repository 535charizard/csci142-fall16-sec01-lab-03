package testcases;
import static org.junit.Assert.*;
import java.util.Iterator;
import org.junit.Test;
import structures.LinkedList;

    public class testRemove {
    private LinkedList link;
	  @Test
		public void removeOneNode()
		{
			link = new LinkedList();
			link.addLast("Abc");
			link.addLast("Bbb");
			link.addLast("Ccc");
			link.remove("Abc");
			
			assertTrue("Didn't return Size of 2",link.size() == 2);
		}
		
		@Test
		public void removeManyNode()
		{
			 link = new LinkedList();
			 link.addLast("Abc");
			 link.addLast("Bbb");
			 link.addLast("Ccc");
			 link.addFirst("Ddd");
			 link.addFirst("Eee");
			 link.addLast("Fff");
			 link.addLast("Ggg");
			 
			 link.remove("Abc");
			 link.remove("Bbb");
			 link.remove("Fff");
			
			 assertTrue("Didn't return Size of 4", link.size() == 4);
		}


		@Test
		public void removeFirstOneNode()
		{
			 link = new LinkedList();
		     link.addLast(1);
			 link.addLast(2);
			 link.addLast(3);
			 link.addLast(4);
			 link.removeFirst();
		
			 assertFalse("This value is still in the list",link.contains(1));
		}
		
		@Test
		public void removeFirstThreeNodes()
		{
			link = new LinkedList();
		    link.addLast(1);
			link.addLast(2);
			link.addLast(3);
			link.addLast(4);
			link.addLast(5);
			link.addLast(6);
			link.addLast(7);
			
			link.removeFirst();
			link.removeFirst();
			link.removeFirst();
			
			assertFalse("This value is still in the list",link.contains(1));
			assertFalse("This value is still in the list",link.contains(2));
			assertFalse("This value is still in the list",link.contains(3));
			
		}
		
		@Test
		public void removeFirstManyNodes()
		{
			link = new LinkedList();
		    link.addLast(12);
			link.addLast(22);
			link.addLast(32);
			link.addLast(42);
			link.addLast(52);
			link.addLast(62);
			link.addLast(72);
			link.addLast(82);
			link.addLast(92);
			link.addLast(112);
			link.addLast(222);
			link.addLast(332);
			link.addLast(442);
			link.addLast(552);
			
			link.removeFirst();
			link.removeFirst();
			link.removeFirst();
			link.removeFirst();
			link.removeFirst();
			link.removeFirst();
			link.removeFirst();
			
			assertFalse("This value is still in the list",link.contains(12));
			assertFalse("This value is still in the list",link.contains(22));
			assertFalse("This value is still in the list",link.contains(32));
			assertFalse("This value is still in the list",link.contains(42));
			assertFalse("This value is still in the list",link.contains(52));
			assertFalse("This value is still in the list",link.contains(62));
			assertFalse("This value is still in the list",link.contains(72));
			
		}
		@Test
		public void removeLastOneNode()
		{
			link = new LinkedList();
		    link.addLast("I");
			link.addLast("am");
			link.addLast("The");
			link.addLast("best");
			link.removeLast();
			
			assertFalse("This value is still in the list",link.contains("best"));
		}
		
		@Test
		public void removeLastThreeNodes()
		{
			link = new LinkedList();
		    link.addLast("I");
			link.addLast("Am");
			link.addLast("The");
			link.addLast("Best");
			link.addLast("Software");
			link.addLast("Developer");
			link.addLast("In");
			link.addLast("the");
			link.addLast("World");
			
		    link.removeLast();
			link.removeLast();
			link.removeLast();
			
			assertFalse("This value is still in the list",link.contains("World"));
			assertFalse("This value is still in the list",link.contains("the"));
			assertFalse("This value is still in the list",link.contains("In"));
		}
		
}
