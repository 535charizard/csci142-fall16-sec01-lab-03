package testcases;

import static org.junit.Assert.*;
import org.junit.Test;
import structures.LinkedList;

public class testGetters {
	private LinkedList link;
	
	@Test
	public void getFirstOneNode()
	{
        link = new LinkedList();
        link.addFirst("I");
		assertTrue("This value does not equall I",link.getFirst().equals("I"));
	}
	
	
	@Test
	public void getFirstFromThreeNodes()
	{
		link = new LinkedList();
	    link.addLast("I");
		link.addLast("Am");
		link.addLast("A");
		link.addLast("College");
		link.addLast("Student");
		//assertTrue("This value does not equall I",link.getFirst().equals("I"));
		//assertTrue("This value does not equall I",link.getFirst().equals("Am"));
		//assertTrue("This value does not equall I",link.getFirst().equals("A"));
		//assertTrue("This value does not equall I",link.getFirst().equals("College"));
		assertTrue("This value does not equall I",link.getFirst().equals("Student"));

	}
	
	@Test
	public void getFirstAfterRemovingTheHead()
	{
		link = new LinkedList();
	    link.addLast("I");
		link.addLast("Am");
		link.addLast("A");
		link.addLast("College");
		link.addLast("Student");
		link.remove("I");
		assertTrue("This value does not equall I",link.getFirst().equals("Am"));
	}
	
	@Test
	public void getFirstAfterAddingANode()
	{
		link = new LinkedList();
	    link.addLast("I");
		link.addLast("Am");
		link.addLast("A");
		link.addLast("College");
		link.addLast("Student");
		link.addFirst("Rick");
		assertFalse("This value does not equall I",link.getFirst().equals("I"));
		assertFalse("This value does not equall I",link.getFirst().equals("Am"));
		assertFalse("This value does not equall I",link.getFirst().equals("A"));
		assertFalse("This value does not equall I",link.getFirst().equals("College"));
		assertFalse("This value does not equall I",link.getFirst().equals("Student"));

	}

	

	@Test
	public void getLastFromOneNode()
	{
		link = new LinkedList();
	    link.addLast("I");
		assertTrue("This value does not equall I",link.getLast().equals("I"));
	}
	
	public void getLastFromThreeNodes()
	{
		link = new LinkedList();
	    link.addLast("I");
		link.addLast("Am");
		link.addLast("A");
		link.addLast("College");
		link.addLast("Student");
		assertTrue("This value does not equall I",link.getLast().equals("Student"));
	}
	
	@Test
	public void getLastAfterRemovingTheHead()
	{
		link = new LinkedList();
	    link.addLast("I");
		link.addLast("Am");
		link.addLast("A");
		link.addLast("College");
		link.addLast("Student");
		link.remove("Student");
		assertTrue("This value does not equall I",link.getLast().equals("College"));
	}
	
	@Test
	public void getLastAfterAddingANode()
	{
		link = new LinkedList();
	    link.addLast("I");
		link.addLast("Am");
		link.addLast("A");
		link.addLast("College");
		link.addLast("Student");
		assertFalse("This value does not equall I",link.getLast().equals("Rick"));
		link.addLast("Rick");
		assertTrue("This value does not equall Am",link.getLast().equals("Rick"));
	}
	
}
