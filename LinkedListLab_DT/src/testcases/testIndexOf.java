package testcases;

import static org.junit.Assert.*;
import java.util.Iterator;
import org.junit.Test;
import structures.LinkedList;

public class testIndexOf {
	private LinkedList link;
	
	public void addFirstToAllFiveNodes()
	    {
	    	link = new LinkedList();
	  	    link.addFirst("Abc");
	  		link.addFirst("Bbb");
	  		link.addFirst("Ccc");
	  		link.addFirst("Ddd");
	  		link.addFirst("Eee");
	  		assertTrue("The first Object is not Eee",link.indexOf("Eee") == 0);
	  		assertFalse("The first Object is Abc",link.indexOf("Abc") == 0);
	  		assertFalse("The first Object is Bbb",link.indexOf("Bbb") == 0);
	  		assertFalse("The first Object is not Ccc",link.indexOf("Ccc") == 0);
	  		assertFalse("The first Object is not Ddd",link.indexOf("Ddd") == 0);
	    }
		
		
	    @Test
	    public void addFirstRemoveFirstAddFirst()
	    {
	    	link = new LinkedList();
	  	    link.addFirst("Abc");
	  		link.addFirst("Bbb");
	  		link.addFirst("Ccc");
	  		link.addFirst("Ddd");
	  	    assertTrue("The first Object is not Ddd",link.indexOf("Ddd") == 0);
	  		link.removeFirst();
	  		assertFalse("The first Object is Ddd",link.indexOf("Ddd") == 0);
	  		link.addFirst("Ddd");
	  	    assertTrue("The first Object is not Ddd",link.indexOf("Ddd") == 0);	
	    }
		
	    @Test
	    public void addFirstWithOne()
	    {
	    	link = new LinkedList();
	  	    link.addLast("Abc");
	  		link.addLast("Bbb");
	  		link.addFirst("Ccc");
	  		assertTrue("The first Object is not Ccc",link.indexOf("Ccc") == 0);
	    }
	    
	    @Test
		public void indexOfThreeNodes()
		{
			link = new LinkedList();
			link.addLast("A");
			link.addLast("B");
			link.addLast("C");
			
			assertTrue("This doesn't returns the int value 0 for the element ",link.indexOf("A") == 0);
			assertTrue("This doesn't returns the int value 1 for the element ",link.indexOf("B") == 1);
			assertTrue("This doesn't returns the int value 3 for the element ",link.indexOf("C") == 2);
		}
		
		@Test
		public void indexOfOneNode()
		{
	        link = new LinkedList();
			link.addLast(12);
			link.addLast(13);
			link.addLast(14);
			
			assertTrue("This doesn't returns the int value 2 for the element ",link.indexOf(14) == 2);
		}

		@Test
		public void indexOfManyNodes()
		{
			 link = new LinkedList();
		     link.addLast(1);
			 link.addLast(2);
			 link.addLast(3);
			 link.addLast(4);
			 link.addLast(5);
			 link.addLast(6);
			 link.addLast(7);
			 link.addLast(8);
			 link.addLast(9);
			
			 assertTrue("This doesn't returns the int value 2 for the element ",link.indexOf(3) == 2);
			 assertTrue("This doesn't returns the int value 2 for the element ",link.indexOf(4) == 3);
			 assertTrue("This doesn't returns the int value 2 for the element ",link.indexOf(5) == 4);
			 assertTrue("This doesn't returns the int value 2 for the element ",link.indexOf(6) == 5);
			 assertTrue("This doesn't returns the int value 2 for the element ",link.indexOf(7) == 6);
		}
}
