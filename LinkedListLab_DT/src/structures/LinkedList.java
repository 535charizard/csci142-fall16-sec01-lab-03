package structures;

/**
 *   A class to provide basic methods of a singly linked list.
 *  It does not extend or implement List, Collection or another class
 *  or interface.  
 *
 * @ author DeArvis Troutman 
 * @ author DeArvis Troutman
 * 
 * November 18th at Midnight
 * 
 *The information required to run this class is myHead and mySize.
 */
public class LinkedList
{
    /////////////////////////////
    //         Properties      //
    /////////////////////////////
    private Node myHead;
    private int mySize;
    
    /////////////////////////////
    //         Methods         //
    /////////////////////////////
    
    /**
     *  Default constructor that creates an empty linked list
     *
     *  <pre>
     *  pre:  the linked list is empty
     *  post: the linked list is empty
     *  </pre>
     */
    public LinkedList()
    {
        myHead = null;
        mySize = 0;
    }
    
    /**
     *  Constructor that creates a new linked list with a single 
     *  node storing the object passed in
     *
     *  <pre>
     *  pre:  myHead points to null (the linked list is empty)
     *  post: myHead points to the only node in the linked list,
     *        that node holding the object passed in
     *  </pre>
     *
     *  @param datum an object to be inserted at the head of the
     *         linked list
     */
    public LinkedList(Object datum)  // Takes in an Object
    {
        myHead = new Node(datum);    //Sets that object to the type Node Head
        myHead.setNext(null);        // Everything after Head is null
    }
    
    /**
     *  Adds a node to the head of the linked list; the special
     *  condition of an empty linked list is handled without
     *  special treatment since if myHead points to null, that
     *  simply becomes the next node in the list, immediately
     *  following the new entered node at the head of the list
     *
     *  <pre>
     *  pre:  the linked list may be empty or contain one or
     *        more nodes
     *  post: the linked list contains one more node that has
     *        been added to the beginning of the list
     *  </pre>
     *
     *  @param aNode the node to be entered
     */
    private void addFirst(Node aNode) // takes in a type Node instead of Object
    {
        aNode.setNext(myHead);       // Sets the inputed Node before the Head
        myHead = aNode;              // Sets the head now equal to the recently
                                     // Inputed node    
        mySize++;
    }
    
    /**
     *  Adds a node to the head of the linked list; the special
     *  condition of an empty linked list is handled without
     *  special treatment since if myHead points to null, that
     *  simply becomes the next node in the list, immediately
     *  following the new entered node at the head of the list
     *
     *  <pre>
     *  pre:  the linked list may be empty or contain one or
     *        more nodes
     *  post: the linked list contains one more node that has
     *        been added to the beginning of the list
     *  </pre>
     *
     *  @param datum the object used to creat a new node to be 
     *         entered at the head of the list
     */
    public void addFirst(Object datum)  //Takes in An Object 
    {
        Node aNode;                     // Initializes a Node
        
        aNode = new Node(datum);        // Initializes aNode to datum
        this.addFirst(aNode);           // The object datum(which is part of the Node)
                                        // goes through addFirst(Node) function and 
        								// makes it the head
        mySize++;
    }
    
    /**
     *  Adds a node to the tail of the linked list; the special
     *  condition of an empty linked list is handled separately
     *
     *  <pre>
     *  pre:  the linked list may be empty or contain one or
     *        more nodes
     *  post: the linked list contains one more node that has
     *        been added to the end of the list
     *  </pre>
     *
     *  @param aNode the node to be entered
     */
    private void addLast(Node aNode)
    {
        Node lastNode;                       //Creates lastNode of type Node
        
        if(myHead==null)                     //if there is no head
        {
            this.addFirst(aNode);            //Make the inputted Node the Head
        }
        else
        {
            lastNode = this.getPrevious(null); //
            lastNode.setNext(aNode);
            aNode.setNext(null);
        }
        mySize++;
        System.out.println("Adding");
    }
    
    /**
     *  Adds a node to the tail of the linked list; the special
     *  condition of an empty linked list is handled separately
     *
     *  <pre>
     *  pre:  the linked list may be empty or contain one or
     *        more nodes
     *  post: the linked list contains one more node that has
     *        been added to the end of the list
     *  </pre>
     *
     *  @param datum the object used to creat a new node to be 
     *         entered at the tail of the list
     */
    public void addLast(Object datum)       
    {
        Node aNode;
        
        aNode = new Node(datum);
        this.addLast(aNode);            //sets datum to type null and 
        mySize++;                       //goes through previous function
        
    }
    
    /**
     *  Deletes a node from the list if it is there
     *
     *  <pre>
     *  pre:  the list has 0 or more nodes
     *  post: if the node to be deleted is in the list,
     *        the node no longer exists in the list; the
     *        node previous to the node to be deleted now
     *        points to the node following the deleted node
     *  </pre>
     *
     *  @param aNode the node to be deleted from the list
     *
     *  @return boolean indicating whether or not the node
     *          was deleted
     */
    private boolean remove(Node aNode)
    {
    	Node currentNode;
    	Node prevNode;
    	currentNode = myHead;
               
      //If LinkedList is Empty return false
        if(currentNode == null)
        {
            System.out.println("Nothing to Remove");
            return false;
        }

        //If aNode is the head
        if( currentNode.equals(aNode) )
        {
            myHead = myHead.getNext();
            mySize--;
            return true;
        }
                  //If finds Node in List
        while(currentNode != null)
        {
        	currentNode = currentNode.getNext();
            if(currentNode.equals(aNode))
            {
                prevNode = getPrevious(currentNode);
                prevNode.setNext(currentNode.getNext());
                mySize--;
                return true;
            }
        }
        return false;
    	
    }
    
    /**
     *  Deletes a node from the list if it is there
     *
     *  <pre>
     *  pre:  the list has 0 or more nodes
     *  post: if the node to be deleted is in the list,
     *        the node no longer exists in the list; the
     *        node previous to the node to be deleted now
     *        points to the node following the deleted node
     *  </pre>
     *
     *  @param datum the object to be deleted from the list
     *
     *  @return boolean indicating whether or not the node
     *          was deleted
     */
    public boolean remove(Object datum)  //My method
    {
    	Node currentNode;
    	Node prevNode;
    	Node aNode = new Node(datum);
    	currentNode = myHead;
        
        if(currentNode == null)
        {
       	System.out.println("Nothing to Remove");
       	return false;
        }

       //If aNode is the head
       if( currentNode.equals(aNode) )
       {
          myHead = myHead.getNext();
          mySize--;
          return true;
       }
                 //If finds Node in List
       while(currentNode != null)
       {
       	currentNode = currentNode.getNext();
           if(currentNode.equals(aNode))
           {
               prevNode = getPrevious(currentNode);
               prevNode.setNext(currentNode.getNext());
               mySize--;
               return true;
           }
     
       }
       return false;
   	
    }
    
    /**
     *  Find a node in the list with the same data as that passed in 
     *
     *  <pre>
     *  pre:  the list has 0 or more nodes
     *  post: list is unchanged
     *  </pre>
     *
     *  @param datum the object for which a node is to be found 
     *         in the list
     *
     *  @return null if a node with the given object datum is not in
     *          the list, or the node if it does
     */
    private Node findNode(Object datum)
    {
        Node currentNode;
        Object currentDatum;
        
        currentNode = myHead;
        currentDatum = null;
        
        while(currentNode != null)
        {
            currentDatum = currentNode.getData();
            if(currentDatum.equals(datum))
            {
                return currentNode;
            }
            currentNode = currentNode.getNext();
        }
        return null;
    }
    
    /**
     *  Determine if a node exists in the list with the same 
     *  data as that passed in 
     *
     *  <pre>
     *  pre:  the list has 0 or more nodes
     *  post: list is unchanged
     *  </pre>
     *
     *  @param datum the object for which a node is to be found 
     *         in the list
     *
     *  @return false if a node with the given object datum is not in
     *          the list, or true if it does
     */
    public boolean contains(Object datum)
    {
    	Node currentNode;
        Object currentDatum;
        
        currentNode = myHead;
        currentDatum = null;
        
        while(currentNode != null)
        {
            currentDatum = currentNode.getData();
            if(currentDatum.equals(datum))
            {
                return true;
            }
            currentNode = currentNode.getNext();
        }
        return false;
    }
    
    /**
     *  Determines the node that resides one closer to the
     *  head of the list than the node passed in
     *
     *  <pre>
     *  pre:  the list has 0 or more nodes
     *  post: the list is unchanged
     *  </pre>
     *
     *  @param aNode the node whose predecessor is being looked for
     *
     *  @return the node that resides one closer to the head of the
     *          list than the node passed in
     */
    private Node getPrevious(Node aNode)          
    {
        Node currentNode;
        
        currentNode = myHead;
        
        if(currentNode == aNode)
        {
            return null;
        }
        
        while(currentNode!=null && currentNode.getNext()!=aNode)
        {
            currentNode = currentNode.getNext();
        }
        
        return currentNode;
    }
    
    /**
     *  A new node is entered into the list immediately before
     *  the designated node
     *
     *  <pre>
     *  pre:  the list may have 0 or more nodes in it
     *  post: if the beforeNode is not in the list, no change
     *        takes place to the list; otherwise, the new
     *        node is entered in the appropriate place
     *  </pre>
     *
     *  @param aNode the node to be entered into the list
     *  @param beforeNode the node before which the new node
     *         is to be entered
     *
     *  @return boolean designating if the node was or was not
     *          entered into list
     */
    //Before Node is the one your adding in, anode is already in the list AFTER
    //Before Node
    private boolean insertBefore(Node aNode, Node beforeNode)
    {
        // aNode is the new Node being entered into the singly linked list
        // BeforeNode is the Node that will be before this Node.
    	Node currentNode =myHead;
    	Node pNode = getPrevious(currentNode);
    	
    	// If linked list is empty, make beforeNode the head
    	if(currentNode.equals(null) || aNode.equals(null))
    	{
    		myHead.equals(beforeNode);
    		return true;
    	}
    	
    	while(currentNode != aNode)
        {
	        currentNode.getNext();
	        if(currentNode.equals(aNode))
	        {
	        	pNode.setNext(beforeNode);
	        	return true;
	        }
        }
    	return false;
    }
    
    /**
     *  A new node with datum is entered into the list immediately
     *  before the node with beforeDatum, the designated node
     *
     *  <pre>
     *  pre:  the list may have 0 or more nodes in it
     *  post: if the node with beforeDatum is not in the list, 
     *        no change takes place to the list; otherwise, a new
     *        node is entered in the appropriate place with the 
     *        object datum
     *  </pre>
     *
     *  @param datum the object used to create the new node 
     *         to be entered into the list
     *  @param beforeDatum the datum of the node before which the 
     *         new node is to be entered
     *
     *  @return boolean designating if the node was or was not
     *          entered
     */
    public boolean insertBefore(Object datum, Object beforeDatum)
    {
    	// aNode is the new Node being entered into the singly linked list
        // BeforeNode is the Node that will be before this Node.
    	Node aNode = new Node(datum);
    	Node beforeNode = new Node(beforeDatum);
    	
    	Node currentNode =myHead;
    	Node pNode = getPrevious(currentNode);
    	
    	// If linked list is empty, make beforeNode the head
    	if(currentNode.equals(null) || aNode.equals(null))
    	{
    		myHead.equals(beforeNode);
    		mySize++;
    		return true;
    	}
    	
    	while(currentNode != aNode)
        {
	        currentNode.getNext();
	        if(currentNode.equals(aNode))
	        {
	        	pNode.setNext(beforeNode);
	        	mySize++;
	        	return true;
	        }
        }
    	return false;
    }
    
    /**
     *  print the list by converting the objects in the list
     *  to their string representations
     *
     *  <pre>
     *  pre:  the list has 0 or more elements
     *  post: no change to the list
     *  </pre>
     */
    public String toString()
    {
        String string;
        Node currentNode;
        
        currentNode = myHead;
        
        string = "head ->";
        
        while(currentNode!=null)
        {
            string += currentNode.getData().toString()+ " -> ";
            currentNode = currentNode.getNext();
        }
        string += "|||";
        return string;
    }

    // ALSO!  Comment and implement the following methods.
    //   !!!

    public int indexOf(Object o)
    {
     Node currentNode = myHead;
     Node aNode = new Node(o);
     int value = 0;
     
     if(currentNode.equals(null) || currentNode.equals(aNode))
     {
    	 return value;
     }
     
     while(currentNode != aNode)
     {
    	currentNode.getNext();
    	value = value + 1;
    	if(currentNode.equals(aNode))
    	{
    		 return value;
    	}
     }
     
     return value;
    }

    public Object removeFirst()
    {
       Node currentNode = myHead;
       
       if(currentNode.equals(null))
       {
    	   return null;
       }
    	
       if(currentNode != null )
       {
    	   currentNode.getNext();
    	   myHead = currentNode;
    	   mySize--;
    	   return myHead;
       }
           return null;
    }
    
    public Object removeLast()
    {
        Node currentNode = myHead;
        Node prevNode = null;
        
        if(currentNode.equals(null) || myHead == null)
        {
        	System.out.println("Nothing to Remove");
        	return null;
        }
        
        while(currentNode != null)
        {
        	currentNode.getNext();
        	if(currentNode.equals(null))
        	{
        		prevNode = getPrevious(currentNode); // Go back One Node from Null
        		prevNode = getPrevious(prevNode);  // Go back One more Node from Null
        		prevNode.setNext(currentNode);
        		mySize--;
                 return true;
            }
        }
    	return null;
    }

    public int size()   // Simple return size of the
    {					// Should only be a few lines of code
    
        return mySize;
    }

    public Object getFirst()
    {
        return myHead;
    }
    
    public Object getLast()
    {
        Node currentNode = myHead;
        while(currentNode != null )
        {
        	currentNode.getNext();
        	if(currentNode.equals(null))
        	{
        		currentNode = getPrevious(currentNode);
        		return currentNode;
        	}
        }
    	return null;
    }

    public void setFirst(Object o)
    {
        Node aNode = new Node(0);
        myHead = aNode;
    }
    
    private void setHead(Node aNode)
    {
        myHead = aNode;
    }
    
    private Node getHead()
    {
        return myHead;
    }
    
    
  /**
 * A class to provide basic methods of a linked list node.
 *
 * @ author DeArvis Troutman 
 * @ author DeArvis Troutman
 * 
 * November 18th at Midnight
 * 
 *The information required to run this class is myData and myNext.
 */
private class Node
{
    ///////////////////////////////////
    //           Properties          //
    ///////////////////////////////////
    private Object myData;
    private Node myNext;
    
    ///////////////////////////////////
    //             Methods           //
    ///////////////////////////////////
    
    /**
     *  Default constructor for a node with null
     *  data and pointer to a next node
     */
    public Node()
    {
        myData = null;
        myNext = null;
    }
    
    /**
     *  Constructor for a node with some object for
     *  its data and null for a pointer to a next node
     *
     *  <pre>
     *  pre:  a null node
     *  post: a node with some object for its data and
     *        null for a pointer to a next node
     *  </pre>
     *
     *  @param datum an object for the node's data
     */
    public Node(Object datum)
    {
        myData = datum;
        myNext = null;
    }
    
    /**
     *  Constructor for a node with some object for 
     *  its data and a pointer to another node
     *
     *  <pre>
     *  pre:  a null node
     *  post: a node with some object for its data and
     *        a pointer to a next node
     *  </pre>
     *
     *  @param datum an object for the node's data
     *  @param next the node that this node points to
     */
    public Node(Object datum, Node next)
    {
        myData = datum;
        myNext = next;
    }
    
    public void setData(Object datum)
    {
        myData = datum;
    }
    
    public Object getData()
    {
        return myData;
    }
    
    public void setNext(Node next)
    {
        myNext = next;
    }
    
    public Node getNext()
    {
        return myNext;
    }
}}
    